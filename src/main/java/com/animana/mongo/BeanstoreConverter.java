package com.animana.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.collections.IteratorUtils;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * @author echyrski.
 */
public class BeanstoreConverter {
    private static final Logger logger = Logger.getLogger(Main.class);

    public static void convert(Properties properties, boolean overrideExistingCollections) {
        int threadCount = Integer.parseInt(properties.getProperty(ConverterPropertes.PROCESSOR_THREAD_COUNT));
        int batchSize = Integer.parseInt(properties.getProperty(ConverterPropertes.FLUSH_BATCH_SIZE));
        MongoClientURI mongoClientSourceURI = new MongoClientURI(properties.getProperty(ConverterPropertes.SOURCE_MONGO_URL));
        MongoClientURI mongoClientTargetURI = new MongoClientURI(properties.getProperty(ConverterPropertes.SOURCE_MONGO_URL));
        MongoClient sourceMongoClient = new MongoClient(mongoClientSourceURI);
        MongoClient targetMongoClient = new MongoClient(mongoClientTargetURI);
        MongoDatabase source = sourceMongoClient.getDatabase(properties.getProperty(ConverterPropertes.SOURCE_DB_NAME));
        MongoDatabase target = targetMongoClient.getDatabase(properties.getProperty(ConverterPropertes.TARGET_DB_NAME));
        if (overrideExistingCollections) {
            target.drop();
        } else {
            checkExistingCollections(target, overrideExistingCollections);
        }
        ThreadPoolExecutor executor = new ThreadPoolExecutor(threadCount, threadCount, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
        List<Future<FutureResp>> futures = new ArrayList<>();
        TimeUtils.start();
        Iterator<Document> sourceCollection = source.listCollections().iterator();
        while (sourceCollection.hasNext()) {
            Document next = sourceCollection.next();
            String name = (String) next.get("name");
            if (name.startsWith("system")) continue;
            futures.add(executor.submit(new CollectionConversionCallable(source, target, name, batchSize)));
        }
        awaitCompletion(futures);
        executor.shutdown();
        TimeUtils.end("Conversion complete");
    }

    private static void checkExistingCollections(MongoDatabase target, boolean allowOverride) {
        List<String> existingtargetCollections = IteratorUtils.toList(target.listCollectionNames().iterator());
        if (existingtargetCollections.size() > 0) {
            logger.warn("Target database is not empty. Following collections exists:");
            existingtargetCollections.forEach(logger::warn);
            if (!allowOverride) {
                logger.error("Please use -o swith to override the database. This action can cause to data loss!");
                System.exit(-3);
            }
        }


    }

    private static void awaitCompletion(List<Future<FutureResp>> futures) {
        for (Future<FutureResp> future : futures) {
            try {
                FutureResp resp = future.get();
                if (resp != null) {
                    logger.error(String.format("Error occured: %s while proessing collection %s ", resp.getException().getMessage(), resp.getCollectionName()));
                }
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e, e);
            }
        }
    }
}
