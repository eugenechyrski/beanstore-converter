package com.animana.mongo;

/**
 * @author echyrski
 */
public class FutureResp {

    private Exception exception;
    private String collectionName;

    public FutureResp(Exception exception, String collectionName) {
        this.exception = exception;
        this.collectionName = collectionName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public Exception getException() {
        return exception;
    }

}
