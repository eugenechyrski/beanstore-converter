package com.animana.mongo;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * @author echyrski
 */
public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);

    public static void main(String... args) throws Exception {
        if (args.length < 2) showUsageAnExit();
        boolean overrideExistingDB = false;
        String propsPath = null;
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            switch (arg) {
                case "-o":
                    overrideExistingDB = true;
                    break;
                case "-f":
                    if (args.length <= i + 1) {
                        showUsageAnExit();
                    } else {
                        propsPath = args[i + 1];
                        i++;
                    }
            }
        }
        File propsFile = new File(propsPath);
        if (!propsFile.exists() || !propsFile.isFile() || propsFile.length() == 0) {
            logger.error("File " + propsFile.getAbsolutePath() + " doesn't exists or is emty");
            showUsageAnExit();
        }
        Properties props = new Properties();

        props.load(new FileReader(propsFile));
        if (!validateProperties(props)) {
            System.exit(-2);
            ;
        }
        BeanstoreConverter.convert(props, overrideExistingDB);
        System.exit(0);
    }

    public static boolean validateProperties(Properties props) {
        boolean valid = true;
        valid = valid & validateProperty(ConverterPropertes.FLUSH_BATCH_SIZE, props);
        valid = valid & validateProperty(ConverterPropertes.PROCESSOR_THREAD_COUNT, props);
        valid = valid & validateProperty(ConverterPropertes.SOURCE_MONGO_URL, props);
        valid = valid & validateProperty(ConverterPropertes.TARGET_MONGO_URL, props);
        valid = valid & validateProperty(ConverterPropertes.SOURCE_DB_NAME, props);
        valid = valid & validateProperty(ConverterPropertes.TARGET_DB_NAME, props);
        return valid;
    }

    public static boolean validateProperty(String name, Properties props) {
        if (props.getProperty(name) == null) {
            logger.error("Property " + name + " is not defined!");
            return false;
        }
        return true;
    }

    public static void showUsageAnExit() {
        System.out.println("java -jar beanstoreConerter.jar -f <Properties file> [-o]");
        System.out.println("\t -f <Properties file>\tPath to properties file");
        System.out.println("\t -o\t\t\t\t\t\tDrop target database if exists" +
                                   "");
        System.exit(-1);
    }


}
