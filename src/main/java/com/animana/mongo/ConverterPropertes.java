package com.animana.mongo;

/**
 * @author echyrski .
 */
public class ConverterPropertes {
    public static final String SOURCE_MONGO_URL = "source.mongo.url";
    public static final String TARGET_MONGO_URL = "target.mongo.url";
    public static final String PROCESSOR_THREAD_COUNT = "thread.count";
    public static final String SOURCE_DB_NAME = "source.beanstore.name";
    public static final String TARGET_DB_NAME = "target.beanstore.name";
    public static final String FLUSH_BATCH_SIZE = "batch.size";


}
