package com.animana.mongo;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author echyrski
 */
public class CollectionConversionCallable implements Callable<FutureResp> {
    private static final Logger logger = Logger.getLogger(Main.class);

    private final MongoDatabase source;
    private final MongoDatabase target;
    private final String collectionName;
    private final List<Document> docs = new ArrayList<>();
    private final int batchSize;
    private int recordCnt = 0;

    public CollectionConversionCallable(MongoDatabase source, MongoDatabase target, String collectionName, int batchSize) {
        this.source = source;
        this.target = target;
        this.collectionName = collectionName;
        this.batchSize = batchSize;
    }

    @Override
    public FutureResp call() throws Exception {

        try {
            TimeUtils.start();
            String targetName = collectionName.replace("@" + source.getName() + "@", "@" + target.getName() + "@");
            MongoCollection<Document> collection = target.getCollection(targetName);
            FindIterable<Document> res = source.getCollection(collectionName).find();
            Iterator<Document> docIterator = res.iterator();
            while (docIterator.hasNext()) {
                Document doc = docIterator.next();
                appendDocument(converDocument(doc));
                if (docs.size() % batchSize == 0) {
                    flush(collection);
                }
            }
            flush(collection);
            validate(source.getCollection(collectionName), collection);
            return null;
        } catch (Exception e) {
            return new FutureResp(e, collectionName);
        } finally {
            TimeUtils.end(String.format("Collection %s: converted %d records", collectionName, recordCnt));
        }
    }

    private void appendDocument(Document doc) {
        docs.add(doc);
    }

    private Document converDocument(Document doc) {
        String rec = JSON.serialize(doc).replaceAll(source.getName() + "(?![a-zA-Z])+", target.getName());
        if (rec.toLowerCase().contains(source.getName())) {
            System.out.println(rec.toLowerCase());
        }
        return Document.parse(rec);

    }

    private void flush(MongoCollection<Document> collection) {
        if (docs.size() == 0) return;
        collection.insertMany(docs);
        recordCnt += docs.size();
        docs.clear();
    }

    private void validate(MongoCollection<Document> source, MongoCollection<Document> target) {
        long sourceCount = source.count();
        long targetCount = target.count();
        if (sourceCount != targetCount) {
            throw new RuntimeException(String.format("Record count does not match %d in source versus %d in target", sourceCount, targetCount));
        }
    }
}
