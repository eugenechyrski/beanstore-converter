package com.animana.mongo;

import org.apache.log4j.Logger;

/**
 * @author echyrski
 */
public class TimeUtils {
    private static final Logger logger = Logger.getLogger(Main.class);

    private static final ThreadLocal<Long> startTime = new ThreadLocal<>();

    public static void start() {
        startTime.set(System.currentTimeMillis());
    }

    public static void end(String message) {
        Long start = startTime.get();
        if (start != null) {
            logger.error(message + String.format(" took %.3f sec.", (System.currentTimeMillis() - startTime.get()) / 1000.0));
            startTime.remove();
        }
    }
}
